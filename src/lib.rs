////////////////////////////////////////////////////////////////////////////////
//! A non contiguous `Vec` that optionally holds a value for a given index.
//!
//! It is called a `LowMap` because it maps a given index to a value, and this
//! index must be small as it will be used as the inner `Vec` index.


use std::ops::{Index, IndexMut};
use std::iter::{
    IntoIterator,
    FromIterator,
    FusedIterator,
    ExactSizeIterator,
    DoubleEndedIterator
};

////////////////////////////////////////////////////////////////////////////////
/// A convenient wrapper around a `Vec<Option<T>>`.
///
/// It is called a `LowMap` because it maps a given index to a value, and this
/// index must be small as it will be used as the inner `Vec` index.
///
/// ```
/// # use low_map::LowMap;
/// let mut map = LowMap::new();
/// map.insert(0, "hey");
/// map.insert(2, "hoy");
/// map.insert(3, "foo");
/// map.insert(2, "bar");
///
/// assert_eq!(map.get(0), Some(&"hey"));
/// assert_eq!(map.get(1), None);
/// assert_eq!(map.get(2), Some(&"bar"));
/// assert_eq!(map.get(3), Some(&"foo"));
///
/// map[2] = "hoho";
/// assert_eq!(map.get(2), Some(&"hoho"));
/// ```
#[derive(Clone, Debug)]
pub struct LowMap<T> {
    len: usize,
    vec: Vec<Option<T>>,
}
impl<T> Default for LowMap<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> LowMap<T> {
    /// Constructs a new empty `LowMap<T>`.
    pub fn new() -> Self {
        Self{ len: 0, vec: Vec::new() }
    }

    /// Constructs a new empty `LowMap<T>` with the given `capacity`.
    pub fn with_capacity(capacity: usize) -> Self {
        Self{ len: 0, vec: Vec::with_capacity(capacity) }
    }

    /// Returns the number of stored elements.
    pub fn len(&self) -> usize {
        self.len
    }

    /// Returns the capacity.
    pub fn capacity(&self) -> usize {
        self.vec.capacity()
    }

    /// Inserts an element at position `index` within the vector, returning the
    /// previous value if present.
    pub fn insert(&mut self, index: usize, value: T) -> Option<T> {
        if index >= self.vec.len() {
            self.vec.resize_with(index + 1, || None)
        }
        if !self.contains(index) {
            self.len += 1;
        }
        self.vec[index].replace(value)
    }

    /// Constructs a wrapper that temporarily inserts an element.
    ///
    /// The returned wrapper will undo the insert when dropped.
    pub fn stack_insert(&mut self, index: usize, value: T) -> StackInsert<T> {
        let elem = self.insert(index, value);
        StackInsert {
            inner: self,
            index,
            elem,
        }
    }

    /// Removes and returns the element at position `index` if present.
    pub fn remove(&mut self, index: usize) -> Option<T> {
        if self.contains(index) {
            self.len -= 1;
        }
        self.vec.get_mut(index)?.take()
    }

    /// Returns a reference to the element at position `index` if present.
    pub fn get(&self, index: usize) -> Option<&T> {
        self.vec.get(index)?.as_ref()
    }

    /// Returns a mutable reference to the element at position `index` if
    /// present.
    pub fn get_mut(&mut self, index: usize) -> Option<&mut T> {
        self.vec.get_mut(index)?.as_mut()
    }

    /// Tests whether an element is present at position `index` or not.
    pub fn contains(&self, index: usize) -> bool {
        match self.vec.get(index) {
            Some(elem) => elem.is_some(),
            None       => false,
        }
    }

    /// Finds the first free index to store the given `elem`.
    ///
    /// Has a maxium complexity of *O*(n).
    pub fn push(&mut self, elem: T) -> usize {
        if let Some(index) = self.vec.iter().position(|e| e.is_none()) {
            self.insert(index, elem);
            index
        }
        else {
            self.vec.push(Some(elem));
            self.len += 1;
            self.len
        }
    }

    /// Returns an iterator over the value of stored elements.
    pub fn values(&self) -> Values<T> {
        Values { inner: self.vec.iter(), count: self.len }
    }

    /// Returns a mutable iterator over the value of stored elements.
    pub fn values_mut(&mut self) -> ValuesMut<T> {
        ValuesMut { inner: self.vec.iter_mut(), count: self.len }
    }
    
    /// Returns an iterator over the index and value of stored elements.
    pub fn iter(&self) -> Iter<T> {
        Iter { inner: self.vec.iter().enumerate(), count: self.len }
    }

    /// Returns a mutable iterator over the index and value of stored elements.
    pub fn iter_mut(&mut self) -> IterMut<T> {
        IterMut{
            inner: self.vec.iter_mut().enumerate(),
            count: self.len,
        }
    }
    
    /// Returns an iterator over the index of stored elements.
    pub fn indexes(&self) -> Indexes<T> {
        Indexes { inner: self.vec.iter().enumerate(), count: self.len }
    }

    /// Returns the wrapped vec as a slice.
    pub fn as_slice(&self) -> &[Option<T>] {
        self.vec.as_slice()
    }
}

pub struct Drain<'a, T> {
    inner: std::iter::Enumerate<std::vec::Drain<'a, Option<T>>>,
    count: usize,
}
impl<'a, T> Iterator for Drain<'a, T> {
    type Item = (usize, T);
    fn next(&mut self) -> Option<Self::Item> {
        while let Some((index, elem)) = self.inner.next() {
            if let Some(elem) = elem {
                self.count -= 1;
                return Some((index, elem));
            }
        }
        None
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.count, Some(self.count))
    }
    fn count(self) -> usize {
        self.inner.count();
        self.count
    }
}
impl<'a, T> ExactSizeIterator for Drain<'a, T> {}
impl<'a, T> FusedIterator for Drain<'a, T> {}
impl<'a, T> DoubleEndedIterator for Drain<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        while let Some((index, elem)) = self.inner.next_back() {
            if let Some(elem) = elem {
                self.count -= 1;
                return Some((index, elem));
            }
        }
        None
    }
}

impl<T> From<Vec<Option<T>>> for LowMap<T> {
    fn from(vec: Vec<Option<T>>) -> Self {
        Self{
            len: vec.iter().filter(|e| e.is_some()).count(),
            vec
        }
    }
}

impl<T> From<LowMap<T>> for Vec<Option<T>> {
    fn from(LowMap{vec, ..}: LowMap<T>) -> Self {
        vec
    }
}

pub struct Indexes<'a, T> {
    inner: std::iter::Enumerate<std::slice::Iter<'a, Option<T>>>,
    count: usize,
}
impl<'a, T> Iterator for Indexes<'a, T> {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        while let Some((index, elem)) = self.inner.next() {
            if elem.is_some() {
                self.count -= 1;
                return Some(index);
            }
        }
        None
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.count, Some(self.count))
    }
    fn count(self) -> usize {
        self.inner.count();
        self.count
    }
}
impl<'a, T> FusedIterator for Indexes<'a, T> {}
impl<'a, T> ExactSizeIterator for Indexes<'a, T> {}
impl<'a, T> DoubleEndedIterator for Indexes<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        while let Some((index, elem)) = self.inner.next_back() {
            if elem.is_some() {
                self.count -= 1;
                return Some(index);
            }
        }
        None
    }
}

pub struct IterMut<'a, T> {
    inner: std::iter::Enumerate<std::slice::IterMut<'a, Option<T>>>,
    count: usize,
}
impl<'a, T> Iterator for IterMut<'a, T> {
    type Item = (usize, &'a mut T);
    fn next(&mut self) -> Option<Self::Item> {
        while let Some((index, elem)) = self.inner.next() {
            if let Some(elem) = elem.as_mut() {
                self.count -= 1;
                return Some((index, elem));
            }
        }
        None
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.count, Some(self.count))
    }
    fn count(self) -> usize {
        self.inner.count();
        self.count
    }
}
impl<'a, T> FusedIterator for IterMut<'a, T> {}
impl<'a, T> ExactSizeIterator for IterMut<'a, T> {}
impl<'a, T> DoubleEndedIterator for IterMut<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        while let Some((index, elem)) = self.inner.next_back() {
            if let Some(elem) = elem.as_mut() {
                self.count -= 1;
                return Some((index, elem));
            }
        }
        None
    }
}

pub struct Iter<'a, T> {
    inner: std::iter::Enumerate<std::slice::Iter<'a, Option<T>>>,
    count: usize,
}
impl<'a, T> Iterator for Iter<'a, T> {
    type Item = (usize, &'a T);
    fn next(&mut self) -> Option<Self::Item> {
        while let Some((index, elem)) = self.inner.next() {
            if let Some(elem) = elem.as_ref() {
                self.count -= 1;
                return Some((index, elem));
            }
        }
        None
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.count, Some(self.count))
    }
    fn count(self) -> usize {
        self.inner.count();
        self.count
    }
}
impl<'a, T> FusedIterator for Iter<'a, T> {}
impl<'a, T> ExactSizeIterator for Iter<'a, T> {}
impl<'a, T> DoubleEndedIterator for Iter<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        while let Some((index, elem)) = self.inner.next_back() {
            if let Some(elem) = elem.as_ref() {
                self.count -= 1;
                return Some((index, elem));
            }
        }
        None
    }
}

pub struct Values<'a, T> {
    inner: std::slice::Iter<'a, Option<T>>,
    count: usize,
}
impl<'a, T> Iterator for Values<'a, T> {
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(elem) = self.inner.next() {
            if let Some(elem) = elem.as_ref() {
                self.count -= 1;
                return Some(elem);
            }
        }
        None
    }
}
impl<'a, T> FusedIterator for Values<'a, T> {}
impl<'a, T> ExactSizeIterator for Values<'a, T> {}
impl<'a, T> DoubleEndedIterator for Values<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        while let Some(elem) = self.inner.next_back() {
            if let Some(elem) = elem.as_ref() {
                self.count -= 1;
                return Some(elem);
            }
        }
        None
    }
}

pub struct ValuesMut<'a, T> {
    inner: std::slice::IterMut<'a, Option<T>>,
    count: usize,
}
impl<'a, T> Iterator for ValuesMut<'a, T> {
    type Item = &'a mut T;
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(elem) = self.inner.next() {
            if let Some(elem) = elem.as_mut() {
                self.count -= 1;
                return Some(elem);
            }
        }
        None
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.count, Some(self.count))
    }
    fn count(self) -> usize {
        self.inner.count();
        self.count
    }
}
impl<'a, T> FusedIterator for ValuesMut<'a, T> {}
impl<'a, T> ExactSizeIterator for ValuesMut<'a, T> {}
impl<'a, T> DoubleEndedIterator for ValuesMut<'a, T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        while let Some(elem) = self.inner.next_back() {
            if let Some(elem) = elem.as_mut() {
                self.count -= 1;
                return Some(elem);
            }
        }
        None
    }
}

impl<T> Index<usize> for LowMap<T> {
    type Output = T;
    fn index(&self, index: usize) -> &Self::Output {
        self.get(index).unwrap()
    }
}
impl<T> IndexMut<usize> for LowMap<T> {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        self.get_mut(index).unwrap()
    }
}

pub struct StackInsert<'a, T> {
    inner: &'a mut LowMap<T>,
    index: usize,
    elem: Option<T>,
}
impl<'a, T> Drop for StackInsert<'a, T> {
    fn drop(&mut self) {
        let index = self.index;
        if let Some(elem) = self.elem.take() {
            self.insert(index, elem);
        }
        else {
            self.remove(index);
        }
    }
}
impl<'a, T> std::ops::Deref for StackInsert<'a, T> {
    type Target = LowMap<T>;
    fn deref(&self) -> &Self::Target {
        self.inner
    }
}
impl<'a, T> std::ops::DerefMut for StackInsert<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.inner
    }
}

impl<T> Extend<(usize, T)> for LowMap<T> {
    fn extend<I>(&mut self, iter: I)
    where I: IntoIterator<Item = (usize, T)>
    {
        for (i, e) in iter {
            self.insert(i, e);
        }
    }
}

impl<T> FromIterator<(usize, T)> for LowMap<T> {
    fn from_iter<I>(iter: I) -> Self
    where I: IntoIterator<Item = (usize, T)>
    {
        let iter = iter.into_iter();
        let (min, _) = iter.size_hint();
        let mut map = LowMap::with_capacity(min);
        map.extend(iter);
        map
    }
}

pub struct IntoIter<T> {
    inner: std::iter::Enumerate<std::vec::IntoIter<Option<T>>>,
    count: usize,
}
impl<T> Iterator for IntoIter<T> {
    type Item = (usize, T);
    fn next(&mut self) -> Option<Self::Item> {
        while let Some((index, elem)) = self.inner.next() {
            if let Some(elem) = elem {
                self.count -= 1;
                return Some((index, elem));
            }
        }
        None
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.count, Some(self.count))
    }
    fn count(self) -> usize {
        self.inner.count();
        self.count
    }
}
impl<T> FusedIterator for IntoIter<T> {}
impl<T> ExactSizeIterator for IntoIter<T> {}
impl<T> DoubleEndedIterator for IntoIter<T> {
    fn next_back(&mut self) -> Option<Self::Item> {
        while let Some((index, elem)) = self.inner.next_back() {
            if let Some(elem) = elem {
                self.count -= 1;
                return Some((index, elem));
            }
        }
        None
    }
}

impl<T> IntoIterator for LowMap<T> {
    type Item = (usize, T);
    type IntoIter = IntoIter<T>;
    fn into_iter(self) -> Self::IntoIter {
        IntoIter{
            inner: self.vec.into_iter().enumerate(),
            count: self.len,
        }
    }    
}

// #[cfg(test)]
// mod tests {
//     use super::*;
//     #[test]
//     fn test_1() {
//         let mut map = LowMap::new();
//         map.insert(2, "hey")
//     }
// }