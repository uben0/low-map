# LowMap

A convenient wrapper around a `Vec<Option<T>>`. It abstracts the optional presence of an element as being a non contiguous vector.

```rust
let mut map = LowMap::new();
map.insert(0, "hey");
map.insert(2, "hoy");
map.insert(3, "foo");
map.insert(2, "bar");

assert_eq!(map.get(0), Some(&"hey"));
assert_eq!(map.get(1), None);
assert_eq!(map.get(2), Some(&"bar"));
assert_eq!(map.get(3), Some(&"foo"));

map[2] = "hoho";
assert_eq!(map.get(2), Some(&"hoho"));
```
